package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DemoApplication {

	@GetMapping("/")
	String home() {
		String input = "";
		if (input == "m") {
			return "master is here";
		}
		if (input != "Y" || input != "y") {
			return "Spring is not here!";
		}
		return "Spring is here.";
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}